import { provide, inject } from 'vue'
import type { InjectionKey } from 'vue'

export const pageNumberKey = Symbol() as InjectionKey<Number>