import { createApp } from "vue";
import { DefaultApolloClient } from '@vue/apollo-composable'
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";

const resolvers = {
    Character: {
        isStarred: (parent: any) => {
            const starredCharacters = JSON.parse(localStorage.getItem("starredCharacters")!) || []
            return starredCharacters.includes(parent.id);
        }
    },
    Mutation: {
        updateCharacterStarred: (_: any, variables: { isStarred: any; id: any; }) => {
            const starredCharacters = JSON.parse(localStorage.getItem("starredCharacters")!) || [];
            if(variables.isStarred){
                localStorage.setItem(
                    "starredCharacters",
                    JSON.stringify(starredCharacters.concat([variables.id]))
                );
            } else {
                localStorage.setItem(
                    "starredCharacters",
                    JSON.stringify(starredCharacters.filter((characterId: any) => characterId !== variables.id))
                );
            }
            return {
                __typename: "Character",
                id: variables.id,
                isStarred: variables.isStarred
            };
        }
    }
}

const httpLink = createHttpLink({
    uri: 'https://rickandmortyapi.com/graphql',
  })
  const cache = new InMemoryCache()
  const apolloClient = new ApolloClient({
    link: httpLink,
    cache,
    resolvers: resolvers,
  })

const app = createApp(App);

app.provide(DefaultApolloClient, apolloClient);

app.use(router);

app.mount("#app");